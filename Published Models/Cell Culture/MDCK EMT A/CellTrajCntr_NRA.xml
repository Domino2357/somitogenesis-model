<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour
        
        
This xml file was used to produce        
Figure 3        

Multiscale model of a cell sheet with the YAP-Rac-E cadherin circuit 
The trajectories of cells over 18 hrs and 500 micro-meters distance as grown on nano-ridge arrays (NRAs).

We thank Lutz Brusch for providing a basic cell sheet simulation that we modified and adapted to this project.</Details>
        <Title>CellTrajCntr_NRA</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size value="500, 100, 0" symbol="size"/>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="noflux" boundary="-x"/>
                <Condition type="periodic" boundary="y"/>
                <Condition type="periodic" boundary="-y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="1800" symbol="stoptime"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Analysis>
        <!--    <Disabled>
        <Gnuplotter decorate="true" time-step="50">
            <Terminal name="png"/>
            <Plot>
                <Cells value="Y" flooding="true">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="lemonchiffon"/>
                            <Color value="0.05" color="light-blue"/>
                            <Color value="0.1" color="light-red"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot title="Rac1">
                <Cells value="R" min="0" max="3" flooding="true">
                    <ColorMap>
                        <Color value="0" color="blue"/>
                        <Color value="3" color="red"/>
                        <Color value="4" color="yellow"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot>
                <Cells value="E" min="0" max="2" flooding="true">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="plum"/>
                            <Color value="4" color="blue"/>
                            <Color value="7" color="cyan"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot>
                <Cells value="d.abs" flooding="true">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="skyblue"/>
                            <Color value="0.1" color="violet"/>
                            <Color value="0.2" color="salmon"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Disabled>
                <Plot>
                    <Cells value="Cr" flooding="true">
                        <Disabled>
                            <ColorMap>
                                <Color value="0" color="red"/>
                                <Color value="0" color="red"/>
                            </ColorMap>
                        </Disabled>
                    </Cells>
                </Plot>
            </Disabled>
        </Gnuplotter>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="0">
            <Input>
                <Symbol symbol-ref="cell.id"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot>
                    <Style style="points" point-size="0.5"/>
                    <Terminal terminal="png"/>
                    <X-axis minimum="0.0">
                        <Symbol symbol-ref="dist"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="25">
                        <Symbol symbol-ref="truavspeed2"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="0">
            <Input>
                <Symbol symbol-ref="cell.id"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot>
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="dist"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="1.5">
                        <Symbol symbol-ref="Y"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="1.0">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="300">
                    <Style style="points" point-size="0.05"/>
                    <Terminal terminal="png"/>
                    <X-axis maximum="size.x" minimum="0.0">
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis maximum="size.y" minimum="0.0">
                        <Symbol symbol-ref="cell.center.y"/>
                    </Y-axis>
                    <Color-bar maximum="0.3" palette="rainbow" minimum="0.0">
                        <Symbol symbol-ref="d.abs"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <Logger time-step="10.0">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="100">
                    <Style style="points" point-size="0.5"/>
                    <Terminal terminal="png"/>
                    <X-axis maximum="size.x" minimum="0.0">
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis maximum="size.y" minimum="0.0">
                        <Symbol symbol-ref="cell.center.y"/>
                    </Y-axis>
                    <Color-bar palette="rainbow" minimum="0.0">
                        <Symbol symbol-ref="truavspeed"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph format="dot" include-tags="#untagged" reduced="false"/>
    </Analysis>
    <Global>
        <Constant value="0.1" name="Basal rate of YAP activation" symbol="ky"/>
        <Constant value="1.8" name="E-cadherin-dependent rate of YAP deactivation" symbol="kye"/>
        <Constant value="2" name="Inactivation rate of YAP" symbol="Dy"/>
        <Constant value="0.9" name="Initial activation rate of E-cadherin" symbol="C"/>
        <Constant value="0.9" name="YAP-dependent rate of E-cadherin expression" symbol="ke"/>
        <Constant value="1" name="Dissociation constant of YAP-WT1 transcriptional constant" symbol="K"/>
        <Constant value="1" name="Inactivation rate of E-cadherin" symbol="De"/>
        <Constant value="3" name="Hill coefficient for E-cadherin" symbol="h"/>
        <Constant value="1" name="YAP-dependent rate of Rac1 expression" symbol="kr"/>
        <Constant value="0.5" name="Michaelis-Menten-like constant for Rac1" symbol="Kr"/>
        <Constant value="0.5" name="Degradation rate of Rac1" symbol="Dr"/>
        <Constant value="6" name="Hill coefficient for Rac1" symbol="n"/>
        <Constant value="1" name="Rac activation fraction" symbol="alphaR"/>
        <Constant value="1.8" name="Rac1-dependent rate of YAP activation" symbol="kyr"/>
        <!--    <Disabled>
        <Constant value="2" symbol="A2" name="basal adhesion">
            <Annotation>E-cad blocking</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="12" name="Max E-cadherin adhesion constant" symbol="A2">
            <Annotation>Control</Annotation>
        </Constant>
        <Constant value="0.85" name="E-cadherin half &quot;saturation&quot; " symbol="A3"/>
        <Constant value="0.4" name="basal migration" symbol="C1"/>
        <Constant value="4" name="Max Rac1 migration constant" symbol="C2"/>
        <Constant value="3" name="Rac1 half &quot;saturation&quot;" symbol="C3"/>
        <Constant value="100" name="max time for initial leader cells to emerge" symbol="tlim"/>
        <Constant value="0.2" name="fraction of neighbourhood Cr that the cell receives each time Cr spreads to it" symbol="frac"/>
        <!--    <Disabled>
        <Constant value="0.5" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>KD</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="1.2" name="Total YAP (active (Y) + inactive)" symbol="Ytot">
            <Annotation>Control</Annotation>
        </Constant>
        <!--    <Disabled>
        <Constant value="2" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>OE</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="5" name="Total Rac1 (active (R) + inactive)" symbol="Rtot"/>
        <Constant value="0.02" name="parameter that determines probability of Cr spread in each time step" symbol="shareprob"/>
        <Variable value="0.0" name="E-cadherin" symbol="E"/>
        <Variable value="0.0" name="Basal Rac1 activation rate" symbol="Cr"/>
        <!--    <Disabled>
        <Variable value="0.0" symbol="x_edge2"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Mapper name="leftmost edge cell" time-step="1.0">
            <Input value="(M>0)*cell.center.x*(cell.center.x>10) + (M==0)*size.x + (M>0)*size.x*(cell.center.x&lt;=10)"/>
            <Output mapping="minimum" symbol-ref="x_edge2"/>
        </Mapper>
    </Disabled>
-->
    </Global>
    <CellTypes>
        <CellType name="medium" class="medium"/>
        <CellType name="dividingcell" class="biological">
            <VolumeConstraint target="50" strength="1"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint mode="aspherity" target="1" strength="1"/>
            <CellDivision division-plane="major">
                <Condition>(rand_uni(0,1)&lt;0.006*(exp(-time/300)+0.2)) * (cell.center.x&lt;40)</Condition>
                <Triggers/>
                <Annotation>Only cells in the first 40 pixels can divide, regardless of domain size. Before, this was size.x*0.1</Annotation>
            </CellDivision>
            <CellDeath name="delete cells near right domain edge">
                <Condition>(cell.center.x>0.99*size.x)</Condition>
            </CellDeath>
            <DirectedMotion direction="1, 0.0, 0.0" name="cell migration" strength="C1+C2*R/(C3+R)"/>
            <Property value="0" name="YAP" symbol="Y"/>
            <Property value="10" name="E-cadherin" symbol="E"/>
            <Property value="0" name="Rac1" symbol="R"/>
            <Property value="0.001" symbol="Cr"/>
            <Property value="0.0" name="distance from right domain edge" symbol="dist"/>
            <Property value="0.0" name="Sum of instantaneous speeds each time step (times 100)" symbol="avspeed"/>
            <Property value="0.0" name="Speed" symbol="truavspeed"/>
            <Property value="0.0" name="Sum of instantaneous speeds over final 200 time steps (times 100)" symbol="avspeed2"/>
            <Property value="0.0" name="average speed over final 200 time steps (times 100)" symbol="truavspeed2"/>
            <Property value="0.0" name="Contact with medium" symbol="M"/>
            <Property value="0.0" name="number of neighbour cells" symbol="neigh"/>
            <Property value="0.0" name="average neighbourhood Cr" symbol="av"/>
            <PropertyVector value="0.0, 0.0, 0.0" name="speed" symbol="d"/>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.medium.id" scaling="length"/>
                <Output symbol-ref="M" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.dividingcell.id" scaling="cell"/>
                <Output symbol-ref="neigh" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="Cr" scaling="cell"/>
                <Output symbol-ref="av" mapping="average"/>
            </NeighborhoodReporter>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="Y" name="Equation for YAP">
                    <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="E" name="Equation for E-cadherin">
                    <Expression>C - ke*Y^h/(K^h+Y^h) - De*E</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="R" name="Equation for Rac1">
                    <Expression>alphaR*(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
                </DiffEqn>
                <Rule symbol-ref="Cr" name="Equation for Cr spread; at each time step before tlim, cells in contact with the medium have a small chance of being endowed with high Cr; at each time step, each cell has a small chance of having its Cr be augmented by the average neighbourhood Cr (up to a max value); cells at the left edge of the domain retain low Cr">
                    <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*av*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>20)</Expression>
                </Rule>
                <Rule symbol-ref="dist" name="distance from right domain edge">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
                <Rule symbol-ref="avspeed" name="Sum of instantaneous speeds each time step (times 100)">
                    <Expression>avspeed+d.abs*200</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed" name="average speed over the simulation (times 100)">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule symbol-ref="avspeed2" name="Sum of instantaneous speeds over final 100 time steps (times 100)">
                    <Expression>avspeed2 + d.abs*100*(time > stoptime-201)</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed2" name="average speed over 100 time steps (times 100)">
                    <Expression>avspeed2/200</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="Cr">
            <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*0.1*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>size.x/10)</Expression>
        </Rule>
    </Disabled>
-->
            </System>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population size="1" type="dividingcell" name="Initialize cell sheet at left edge of the domain">
            <!--    <Disabled>
        <InitRectangle mode="regular" number-of-cells="70">
            <Dimensions origin="size.x/100, 0, 0" size="size.x/100, size.y, size.z"/>
        </InitRectangle>
    </Disabled>
-->
            <InitRectangle mode="regular" number-of-cells="70">
                <Dimensions size="4.0, size.y, 0.0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact type1="dividingcell" type2="dividingcell" value="30">
                <AddonAdhesion adhesive="A2*E/(A3+E)" name="Adhesive strength" strength="5"/>
            </Contact>
            <Contact type1="dividingcell" type2="medium" value="12"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
