BEGIN { 
 if (ARGC!=5) {
  print "wrong argument number: window (um) max_distance (um) delta_r (um) time (MCS)";
  exit -1;
 }
 window=ARGV[1]*0.5;
 #window=100;
 lmax=ARGV[2];
 delta=ARGV[3];
 t=ARGV[4];
 fin=sprintf("logger_3_%05d.csv", t);
 fout=sprintf("prolif_ratio_%05d.csv", t);
 fgraph=sprintf("prolif_ratio_%05d.png", t);
 getline < fin; 
 while ((getline < fin) > 0) {
  for(i=0;i<lmax/delta+1;i++) {
   #gmin=(i*delta-window);
   # gmax=((i+1)*delta+window);
   #print gmin,gmax;
   if ($4>=(i*delta-window) && $4<((i+1)*delta+window)) {
    s[i]+=$3;
    n[i]++;
    #break;
   }
  } 
 }
 
 print "# distance.to.rim.micrometer","proliferation_ratio" > fout
 for(i=0;i<lmax/delta+1;i++) {
  if (n[i]>0) print i*delta,100*s[i]/n[i] > fout
  else print i*delta,0 > fout
 }

## optional: plot data with gnuplot
print sprintf("set term png size 1024,768; set output \"%s\"; "\
            "set xlabel \"distance to rim (um)\"; "\
            "set ylabel \"ratio of proliferating cells\"; "\
            "set grid; plot \"%s\" wi lines title \"t=%g days\";"\
            ,fgraph,fout,t/24) > "tmp.gpl"
system("gnuplot tmp.gpl");
}
