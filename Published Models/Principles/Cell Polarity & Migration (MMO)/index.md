---
MorpheusModelID: M7682

title: "Mixed-mode Oscillations in a Rac/Rho/Paxillin Subsystem" 

authors: [L. Plazen, J. Al Rahbani, C. M. Brown, A. Khadra]
contributors: [L. Plazen]

# Reference details
publication:
  doi: "10.1038/s41598-023-31042-8"
  title: "Polarity and mixed-mode oscillations may underlie different patterns of cellular migration"
  journal: "Scientific Reports"
  volume: 13
  #issue:
  page: "4223"
  year: 2023
  original_model: true
  preprint: false

tags:
- Cell Motility
- MembraneProperty
- PDE
- CPM

#categories:

---
> Different cell migration patterns may result from different dynamic states like wave pinning (WP), mixed-mode oscillations (MMO) and relaxation oscillations (RO) of the Rho-Rac-paxillin circuit on the cell membrane.

## Introduction

Different patterns of cell migration including directional, exploratory and stationary have been observed for Chinese hamster Ovary (CHO-K1) cells and are related to a mechanistic model of cell polarity ([Plazen *et al.*, 2023](#reference)). 
This model of 4 coupled PDEs for the Rho-Rac-paxillin circuit on the cell membrane generates different cell migration patterns that correspond to different dynamic states of the PDEs like wave pinning (WP), mixed-mode oscillations (MMO) and relaxation oscillations (RO).

## Description

The original model was developed in the Morpheus 2.2 series and can be downloaded as {{< model_quick_access "media/model/m7682/model_2.2.xml" >}}. 
In addition, it has here been updated (added Plots for R, B, kb and swapped order of function declarations for P, Iks inside `CellType/System` to ease initialization) and can be downloaded as {{< model_quick_access "media/model/m7682/model.xml" >}} to run in the latest version of Morpheus (currently the 2.3 series).
The key parameter LK can be changed in `CellType/System`.
Runtime in the paper is typically $8\times10^4$ time steps while the model here stops at $5\times10^3$ time steps to give a quick impression. 

## Results

A typical simulation result is shown below.

![](plot_01000.png)
