---
MorpheusModelID: M2986

title: "Beta Cell Turnover in Pancreatic Islet"
subtitle: How is the right islet size established during zebrafish pancreas development?

authors: [M. Akhtar, et al]
contributors: [L. Brusch, D. Jahn]
submitters: [L. Brusch]
curators: [D. Jahn]

# Under review
hidden: true
private: true

# Reference details
#publication:
#  doi: ""
#  title: "tba"
#  journal: "tba"
#  volume: 0
#  issue: 0
#  page: 0
#  year: 2024
#  original_model: true
#  preprint: false

tags:
- 2D
- Agent-based Simulation
- Beta-Cell
- Cell Death
- Cell Division
- Cell Influx
- Cellular Potts Model
- CPM
- Islet of Langerhans
- Multicellular Model
- Mutant
- ODE Model
- Ordinary Differential Equation
- Pancreas
- Spatial Model
- Stochastic Model
- Wildtype
- Zebrafish

#categories:
#- DOI:
---
> How is the right islet size established during zebrafish pancreas development?

## Introduction

Beta cells in the developing pancreas of zebrafish divide rapidly between 5 and 30 days post fertilization (dpf) and additional beta cells are recruited to the islet. Yet their overall number by 30 dpf remains well under control with the help of cell death. 

## Model Description

This Morpheus model combines four models of beta cell turnover in the pancreatic islet. One pair of models are ordinary differential equations for (the continuum approximation of) the beta cell number, the other pair are two-dimensional agent-based models (in z-layers ‘0’ for wildtype and ‘2’ for the mutant, separated by a fixed layer ‘1’) that explore stochastic effects of the combined cell behaviors and visualize the spatial density of cell death events. Of each pair, one model corresponds to the wildtype with all processes active and the other to the mutant.

## Results

The following movie shows the agent-based simulation:

<figure>
  ![](M2986-pancreas-islet_vid-1_agent-based-models.mp4)
  <figcaption>
    Video of the simulation for wildtype (<strong>left</strong>) and mutant (<strong>right</strong>). Time in dpf is given at the bottom right. <strong>Red</strong> denotes quiescent and <strong>green</strong> cycling beta cells. <strong>Blue</strong> cells undergo cell death.
  </figcaption>
</figure>

Cell numbers from the simulation above are shown in the following graph and compared to ODE simulations with the same rate constants:

![Time courses of the beta cell numbers](M2986-pancreas-islet_fig-1_time-course.png "Time courses of the beta cell numbers compared for wildtype and mutant, both for the ODE and agent-based models. See legend for line color.")
