---
MorpheusModelID: M0035

authors: [Y. Mori, A. Jilkine, L. Edelstein-Keshet]
contributors: [J. Starruß, L. Brusch]

title: "MembraneProperties: Cell Polarization and Chemotaxis"
date: "2019-11-11T16:59:00+01:00"
lastmod: "2022-12-02T13:36:00+01:00"

# Reference details
publication:
  doi: "10.1529/biophysj.107.120824"
  title: "Wave-Pinning and Cell Polarity from a Bistable Reaction-Diffusion System."
  journal: "Biophysical Journal"
  volume: "94(9)"
  page: "3684-3697"
  year: 2008
  original_model: false

aliases: [/examples/membraneproperties-cell-polarization-and-chemotaxis/]

menu:
  Built-in Examples:
    parent: Multiscale Models
    weight: 50
weight: 210
---
## Introduction

This model of cell polarity shows the coupling of three model formalisms:

- A cellular Potts model with ```DirectedMotion``` that responds to cell polarity (but not directly to the external signal gradient),
- a co-moving PDE model on the membrane of the cell (to let cell polarity emerge from distributed receptor-signal interactions) and
- an external signal gradient (implemented as a global field with random fluctuations).

The cell membrane polarizes in response to the external signal _U_ that is sensed as a pattern (of receptor activation) _s_ along the cell membrane.

![](cellpolarity0060.png "Cell dynamically repolarizes in response to switching external gradient.")

## Description

This example implements two models of cell polarity formation: Meinhardt's substrate-depletion model (ASDM) and Edelstein-Keshet's wave-pinning (WP) model. The user can switch the polarity model by ```disabling```/```enabling``` the relevant ```System``` under ```CellType```. Equations and parameter values are copied from [Mori _et al._](#reference).

Each model defines a two-component reaction-diffusion system (of ```MembraneProperty``` _A_ and _B_) representing membrane-bound molecules, and is mapped to and co-moves with a cell. Cell motility and cell shape dynamics are given by the cellular Potts model. An external gradient with random fluctuations, specified in ```Global```, provides a signal for the polarization of the cell. In turn, the pattern of the ```MembraneProperty``` _A_ gives the polarity vector of the cell. The polarity vector then controls the ```DirectedMotion``` behavior of the cell. A ```TiffPlotter``` exports the full 3D plus time data set.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/60636346?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

After a switch in direction of the gradient, the cell re-polarizes in the new direction and starts to move in the new direction, if the wave pinning model has been selected (see video below). A ```GnuPlotter``` visualizes the gradient, cell shape and ```MembraneProperty``` _A_ at a horizontal slice through 3D space (shown below) and a ```Logger``` visualizes the (_phi, theta_)-dependency of the ```MembraneProperty``` _A_ (not shown). 

![](Video_WP.mp4)

The alternative ASDM maintains its previous polarity for much longer and does not re-polarize during the runtime (see video below). On a longer time scale, the ASDM may eventually re-polarize the 3D cell by rotating the activator maximum over the cell membrane. The cell would take a U-turn with a large radius. The same behaviours are observed for any other strength of the gradient's fluctuations as well as for a smooth gradient.

![](Video_ASDM.mp4)
