<MorpheusModel version="4">
    <Description>
        <Title>Actin Waves (PDE) in 1D</Title>
        <Details>Full title:		Actin Waves
Authors:		L. Edelstein-Keshet
Contributors:	Y. Xiao
Date:		23.06.2022
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2013
File type:		Main model
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		Simulations of F-actin negative feedback to the wave-pinning GTPase model produced the actin waves PDE system. Here we simulate this in 1D, showing profiles of the variables over space and time, as well as kymographs of the active GTPase and the F-actin summarizing the entire dynamics.</Details>
    </Description>
    <Global>
        <Field symbol="a" name="activeGTPase" value="1.5+0.7*(0.5-rand_norm(1,0.5))">
            <Diffusion rate="0.01"/>
        </Field>
        <Field symbol="i" name="Inactive" value="2.0">
            <Diffusion rate="1"/>
        </Field>
        <Field symbol="F_a" name="F-actin" value="1.3">
            <Diffusion rate="0.0"/>
        </Field>
        <System time-step="0.05" solver="Runge-Kutta [fixed, O(4)]">
            <DiffEqn symbol-ref="a">
                <Expression> i*(b+gamma*a^n/(1+a^n))- a*(eta+s*F_a) </Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="i">
                <Expression> -i*(b+gamma*a^n/(1+a^n))+ a*(eta+s*F_a)</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="F_a">
                <Expression> epsilon*(k_n*a-k_s*F_a)</Expression>
            </DiffEqn>
            <Constant symbol="b" name="basal activation rate" value="0.067"/>
            <Constant symbol="gamma" name="feedback rate" value="4.5"/>
            <Constant symbol="n" name="Hill coefficient" value="3"/>
            <Constant symbol="k_n" name="Actin nucleation rate" value="20"/>
            <Constant symbol="k_s" name="Actin disassembly rate" value="2"/>
            <Constant symbol="eta" name="basal GTPase decay rate" value="0.5"/>
            <Constant symbol="s" name="actin-dependent GTPase decay rate" value="0.5"/>
            <Constant symbol="F_0" name="actin set point" value="1"/>
            <Constant symbol="epsilon" name="actin reaction rate" value="0.001"/>
        </System>
    </Global>
    <Space>
        <Lattice class="linear">
            <Size symbol="size" value="100, 0, 0"/>
            <BoundaryConditions>
                <Condition type="noflux" boundary="x"/>
            </BoundaryConditions>
            <NodeLength value="0.02"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="6000"/>
        <SaveInterval value="0"/>
        <!--    <Disabled>
        <RandomSeed value="1"/>
    </Disabled>
-->
        <TimeSymbol symbol="time"/>
    </Time>
    <Analysis>
        <Logger time-step="50">
            <Input>
                <Symbol symbol-ref="a"/>
                <Symbol symbol-ref="i"/>
                <Symbol symbol-ref="F_a"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="10" title="space plot">
                    <Style line-width="3.0" style="lines"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="space.x"/>
                    </X-axis>
                    <Y-axis minimum="0" maximum="10">
                        <Symbol symbol-ref="a"/>
                        <Symbol symbol-ref="i"/>
                        <Symbol symbol-ref="F_a"/>
                    </Y-axis>
                    <Range>
                        <Time mode="current"/>
                    </Range>
                </Plot>
                <Plot time-step="-1" title="time-space plot">
                    <Style decorate="true" point-size="2" style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="space.x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="time"/>
                    </Y-axis>
                    <Color-bar>
                        <Symbol symbol-ref="a"/>
                    </Color-bar>
                </Plot>
                <Plot time-step="-1" title="time-space plot">
                    <Style decorate="true" point-size="2" style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="space.x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="time"/>
                    </Y-axis>
                    <Color-bar>
                        <Symbol symbol-ref="F_a"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
