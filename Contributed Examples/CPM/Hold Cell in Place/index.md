---
MorpheusModelID: M5495

title: "Hold cell in place"

authors: [J. Starruß]
contributors: [L. Brusch]
submitters: [L. Brusch]
curators: [L. Brusch]

# Under review
hidden: false
private: false

tags:
- 2D
- Agent-based Simulation
- Cellular Potts Model
- CPM
- DirectedMotion
- FreezeMotion
- Membrane
- MembraneProperty
- Signaling
- Spatial Model
- Stochastic Model

#categories:

---
> Can a CPM cell be held in a defined position and continue its other dynamics?

## Introduction

CPM cells inherently interact with other cells by volume exclusion (steric interaction) and can get pushed out of the way. Here, we compare three cell behaviors where, in addition to steric interactions, either a cell's center of mass or the entire cell shape get fixed. All other dynamics continue inside each cell and on each cell shape. This example was motivated by [Ivan in the Morpheus user forum](https://groups.google.com/g/morpheus-users/c/GMLfPr8wa7g).

## Model Description

This model has two `CellTypes`. The first, our cells of interest, is used to initialize 3 cells in a column. All model elements that control aspects of the cell shape are tagged with `cell`. Just to demonstrate that all other dynamics continue inside each cell, on its membrane and in its interaction with a global field, we also let these cells perform intracellular and membrane dynamics (tagged `membrane`) that interact with an extracellular field (tagged `global`). 

![Snapshot of the three cells of interest](snapshot.png "The three cells of interest are placed on the central column and the runner cell passes from top to bottom on the right lane. The right panel shows the global field that receives a substance, secreted from selected membrane domains.")

The second `CellTypes` gives a single runner cell on the right lane that just aims to perturb the three other cells.

## Results

Based on the cells' IDs, we let cell ID=1 (bottom cell in column and red trajectory in following plot) perform the normal membrane fluctuations of a CPM. Its center of mass fluctuates strongly and gets pushed away 3 lattice units from its initial position.

The cell with ID=2 (middle and green trajectory) stores the initial position of its center of mass and uses `DirectedMotion` in opposite direction from its displacement to restore that position after a perturbation. This yields minute fluctuations of less than a lattice unit. The strength parameter of `DirectedMotion` allows to tune the rigidity of the center of mass position. The cell shape remains flexible.

The cell with ID=3 (top and blue trajectory) freezes its entire shape after an initial time step (the condition to freeze after one time step is chosen such that a short line becomes visible in the trajectory plot). 

The runner cell then squeezes around the frozen cell, reversibly deforms the middle cell (that is held in place) and it pushes aside the normal bottom cell.

![Trajectories of three cells of interest](trajectories.png "Each of the three cells of interest tracks the displacement vector of its center of mass from its initial position. The bottom cell of the following movie has ID=1 and its trajectory is shown in red. The middle cell controls its center of mass and is depicted in green. The top cell has a frozen shape and its center of mass rests, represented by the short blue trajectory.")

![](movie.mp4)

The same method may also be used to force cells along arbitrary (microscopy image-derived) trajectories by inserting data into the calculation of the displacement vector.
