---
MorpheusModelID: M8638

title: "Circle Dance"
#linktitle: ""
subtitle: "Demonstration of dynamically regulated cell-cell adhesion as a function of changing cell properties"

authors: [J. Starruß]
contributors: [J. Starruß, S. Pastor, L. Fischer]
submitters: [S. Pastor, L. Fischer]
curators: [D. Jahn]

# Peer review options
hidden: false
private: false

# date: "2020-05-08T00:00:00+02:00"

#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- Adhesion
- Adhesive
- Adhesive Molecule
- Antagonistic Process
- Attachment
- Cell-Cell Adhesion
- Cell Property
- Delayed Degradation
- Detachment
- Gnuplot
- Homophilic Adhesion
- Neighborhood Reporter
- Oscillation

#categories:
#- ... 
--- 
>Demonstration of dynamically regulated cell-cell adhesion as a function of changing cell properties

## Introduction

This model is an artistic example of cell-state-dependent adhesion.

![](plot_example.png "This is an example from the middle of the simulation. The red color shows a high concentration of the adhesive molecule while the white color demonstrates a low concentration. Correspondingly, red cells attach on the left and the white cells disperse on the right")

## Description

Cells in isolation produce an adhesive molecule which, if sufficiently high, lets the cells attach to each other. An antagonistic process causes delayed degradation of adhesive molecules when cells are actually in contact. The result is an oscillation with repeated attachment and detachment of cells, reminding of a dance.

The adhesive concentration is used in the `HomophilicAdhesion` plugin in the `//CPM/Interaction/Contact` section, i.e. adhesive molecules are required in both neighboring cells to make them adhesive.

Since the `HomophilicAdhesion` plugin looks for the symbol `adhesive_d` in all neighboring nodes of a cell, it must also be declared there, including in the medium. To respond with a default value for `adhesive_d` from medium nodes (or potentially other cell types), we can either declare a default of $`0`$ in `//Global` or specifically in `//CellType/Medium` (done here).

## Results

![A movie visualizing the effect of the adhesion molecules in the cells.](CircleDance.mp4)
**Movie:** Visualization of the effect of the adhesion molecules in the cells

When opening the model in the GUI, you may also switch on the so far disabled second `Gnuplot` with a further state variable (`contact_fraction`) of the process, which is measured per cell by the `NeighborhoodReporter`.