---
MorpheusModelID: M8639

title: "Logistic Proliferation"
#linktitle: ""
subtitle: "Regulation of cell proliferation by shared resource as in a stem cell niche"

authors: [M. Odonkor, L. Brusch]
contributors: [L. Brusch]
submitters: [L. Brusch]
curators: [L. Brusch]

# Peer review options
hidden: false
private: false

# date: "2024-06-15T15:00:00+02:00"

#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- Agent based model
- Proliferation
- Differentiation
- CPM

#categories:
#- ... 
--- 
>How to dynamically regulate individual cell proliferation?

## Introduction

Proliferation of individual cells may be coupled through the dependence on common resources. This turns the otherwise exponential growth of the population size into, for example, logistic growth. Stem cell populations in their niche may be modeled this way such that a bounded population size of stem cells results and generates a constant flux of differentiated cells. The latter scenario was motivated by Mavis in the [User Forum](https://groups.google.com/g/morpheus-users/c/IR4xHp8wGSU) and is picked up in this model.

## Description

![](Logistic_Proliferation.png "Snapshot of the simulation with a stem cell niche (red) and differentiated cells (blue).")

The model considers two cell types, stem cells (red) and differentiated cells (blue). Stem cells proliferate with a rate 'r*(1-N_SC/K)' where 'r' is the maximum division rate of an isolated cell, 'N_SC' is the number of coupled stem cells (provided by Morpheus through the Symbol 'celltype.stem_cell.size') and 'K' is the carrying capacity of the niche. Differentiated cells, just for illustration, shrink their volume and migrate outward. Differentiation occurs with rate 'd'. The parameters 'r', 'K' and 'd' can be set in the 'Global' section of the model. The simulation is initialized with a single stem cell in the center.

## Results

![Visualization of the logistic proliferation of individual cells (red) and differentiation into migrating cells (blue).](Logistic_Proliferation.mp4)
**Movie:** Simulation of the logistic proliferation of individual cells (red) and differentiation into migrating cells (blue).

The number of simulated stem cells can be counted (purple in the time course plot) and shows the emergent balance of proliferation (coupled through a common carrying capacity) and differentiation. Also the number of differentiated cells is counted (green curve) and shows a linear increase after the stem cell population has reached its balance.

![](logger.png "Number of stem cells and differentiated cells in the above simulation movie.")
